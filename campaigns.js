
var router = require('express').Router();
var moment = require('moment');

var Services = require('../../services/');
var Auth = Services.resolve(Services.NAMES.Auth);

var Campaign = require('../../models/campaign');
var CampaignPlacement = require('../../models/campaign_placement');
var EmailTemplate = require('../../models/email_template');

var auth = require('../../lib/auth');
var ApiError = require('../../lib/api_error');
var Validator = require('../../lib/validator');

var DATE_FORMAT = 'YYYY-MM-DD';

/* GET campaigns list. */
router.get('/', auth(['admin']), (req, res, next) => {

  Campaign
    .findAll()
    .then(records => {

      var out = [];

      records.forEach(record => {

        out.push({
          id: record.id,
          userId: record.owner,
          countryId: record.countryId,
          name: record.name,
          startDate: moment(record.startDate).format(DATE_FORMAT),
          endDate: moment(record.endDate).format(DATE_FORMAT),
          status: record.status
        });
      });

      res
        .status(200)
        .json(out);
    }, next);
});

/* GET campaign details. */
router.get('/:id', auth(['admin']), (req, res, next) => {

  return Campaign
    .findById(req.params.id)
    .then(record => {

      if (!record) {

        throw new ApiError(400, 'Campaign not found');
      }

      var out = {
        id: record.id,
        userId: record.owner,
        countryId: record.countryId,
        budget: record.budget,
        name: record.name,
        startDate: moment(record.startDate).format(DATE_FORMAT),
        endDate: moment(record.endDate).format(DATE_FORMAT),
        targetGoalValue: record.targetGoalValue,
        postClickCookieWindow: record.postClickCookieWindow,
        postViewCookieWindow: record.postViewCookieWindow,
        expotaskIO: record.expotaskIO,
        ioId: record.ioId,
        status: record.status
      };

      res
        .status(200)
        .json(out);
    })
    .catch(next);
});

/* POST campaign stat ids */
router.post('/:id/statids', auth(['admin']),
  (req, res, next) => {

  Validator.requireParams(req, ['ioId', 'placements'], 'body');

  var campaign;

  return Campaign
    .findById(req.params.id)
    .then(c => {

      campaign = c;

      if (!campaign) {

        throw new ApiError(400, 'Campaign not found');
      }

      return CampaignPlacement
        .setIds(campaign, req.body);
    })
    .then(result => {

      if (result.firstTime) {

        Auth
          .getUser(req.token, campaign.owner)
          .then(user => {

            EmailTemplate
              .compose(EmailTemplate.TEMPLATES.START_CAMPAIGN)
              .setUser(user)
              .setCampaignId(campaign.id)
              .send();
          });
      }

      res.status(200).json();
    }, next);
});

/* GET campaign stat ids */
router.get('/:id/statids', auth(['admin']),
  (req, res, next) => {

  return Campaign
    .findById(req.params.id)
    .then(campaign => {

      if (!campaign) {

        throw new ApiError(400, 'Campaign not found');
      }

      return CampaignPlacement
        .getIds(campaign);
    })
    .then(data => {

      res.status(200).json(data);
    }, next);
});

module.exports = router;
