
'use strict';

/**
 * @memberOf Middlewares
 * @since 20-04-2016
 * @function super
 * @description identify authnticated user as super user
 * @param {object} req.user - user object
 * */
module.exports = function(req, res, next){

  var superUserEmailPatterns = [
    '@xxxxxxxx.xx$',
    '@xxxxxxxx.xxx$'
  ];

  var isSuperUser = false;
  for(var i = 0; i < superUserEmailPatterns.length; ++i) {

    var pattern = new RegExp(superUserEmailPatterns[i]);
    if (pattern.test(req.user.email)) {
      isSuperUser = true;
      break;
    }
  }

  if (!isSuperUser) {
    res.apiError.status = 403;
    return next(res.apiError.push('permission denied', 'you have not enough permissions to accomplish this action'));
  }

  next();
};
